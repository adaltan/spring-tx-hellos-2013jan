package jhyun.spring_hellos.sqlmaps;

public interface HelloMapper {
    public int countTest();
    public int onePlusOne();
}
