package jhyun.spring_hellos.sqlmaps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("sqlMapApp")
@Component
public class SqlMapsApp implements Runnable {

    private Logger logger = LoggerFactory.getLogger(getClass());
    @Qualifier("helloMapper")
    @Autowired
    private HelloMapper helloMapper;

    @Override
    public void run() {
        logger.debug(String.format("helloMapper = %s", helloMapper));
        logger.info(String.format("1 + 1 = %s", helloMapper.onePlusOne()));
    }
}
