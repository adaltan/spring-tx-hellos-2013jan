package jhyun.spring_hellos.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class App implements Runnable {

    private Logger log = LoggerFactory.getLogger(getClass());
    //@Qualifier("jdbcAndTxHelloApp")
    @Qualifier("sqlMapApp")
    @Autowired
    private Runnable app;

    @Override
    public void run() {
        log.info("run {}", app);
        app.run();
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = null;
        try {
            applicationContext = new ClassPathXmlApplicationContext(
                    "/spring/applicationContext.xml");
            applicationContext.getBean(App.class).run();
        } finally {
            if (applicationContext != null) {
                applicationContext.close();
            }
        }
    }
}
