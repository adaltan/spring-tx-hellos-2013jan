package jhyun.spring_hellos.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("jdbcAndTxHelloApp")
@Component
public class JdbcAndTxHelloApp implements Runnable{
    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcAndTxHelloDao jdbcAndTxHelloDao;

    @Override
    public void run() {
        jdbcAndTxHelloDao.createTestTable();
        jdbcAndTxHelloDao.countTestTable();
        try {
            jdbcAndTxHelloDao.insertTestTablesWithInterFail();
        } catch (Exception ex) {
            log.error("it's ok.", ex);
        }
        final int n = jdbcAndTxHelloDao.countTestTable();
        if (n != 0) {
            log.error("FAIL!");
        }
    }

}
