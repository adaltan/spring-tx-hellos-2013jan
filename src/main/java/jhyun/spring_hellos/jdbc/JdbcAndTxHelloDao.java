package jhyun.spring_hellos.jdbc;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class JdbcAndTxHelloDao {

    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate = null;

    @PostConstruct
    private void postConstruct() {
        // jdbcTemplate
        jdbcTemplate = new JdbcTemplate(dataSource);
        log.debug("jdbc-template {}", jdbcTemplate);
    }

    @Transactional
    public void createTestTable() {
        jdbcTemplate.execute("create table test (id int primary key)");
    }

    @Transactional(readOnly = true)
    public int countTestTable() {
        int n = jdbcTemplate.queryForInt("select count(*) from test");
        log.debug(String.format("count = %s", n));
        return n;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    private int insertTestTable(int id) {
        return jdbcTemplate.update("insert into test (id) values (?)", id);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Throwable.class})
    public void insertTestTablesWithInterFail() throws Exception {
        insertTestTable(1);
        insertTestTable(2);
        insertTestTable(3);
        throw new Exception();
    }
}
